#ifdef DBGFLAGS
#define DEBUG 1
#else
#define DEBUG 0
#endif

#include <err.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum ERROR_CODES
{
    SUCCESS = 0,
    WRONG_ARG,
    INVALID_FILE,
    ERROR,
};

void verify_signature(FILE *data)
{
    uint8_t signature[8] = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };

    uint8_t first[8];
    if ((fread(first, sizeof(first), 1, data)) != 1) // Read less than 8 bytes.
        errx(INVALID_FILE, "given file has too little bytes to read.");

    if (memcmp(first, signature, 8) != 0)
        errx(INVALID_FILE, "the first 8 bytes do not match a png file.");

    if (DEBUG)
    {
        printf("[SIGNATURE] ");
        for (uint8_t i = 0; i < 8; i++)
            printf("%d ", first[i]);
        puts("");
    }
}

void big_to_little_endian(void *value, size_t n)
{
    char *p = value;
    size_t begin = 0;
    size_t end = n - 1;
    for (; end > begin; begin++, end--)
    {
        char tmp = p[begin];
        p[begin] = p[end];
        p[end] = tmp;
    }
}

int get_chunk(FILE *data)
{
    static uint8_t nb_chunk = 1;
    uint32_t size;
    if ((fread(&size, sizeof(size), 1, data)) == 0)
        errx(INVALID_FILE, "chunk %d has an invalid size.", nb_chunk);

    char type_code[4];
    if ((fread(&type_code, sizeof(type_code), 1, data)) == 0)
        errx(INVALID_FILE, "chunk %d has an invalid type code.", nb_chunk);

    // Convert the 32 bits integer read to be able to print it as is.
    big_to_little_endian(&size, sizeof(uint32_t));

    if ((fseek(data, size, SEEK_CUR)) == -1)
        errx(INVALID_FILE, "chunk %d could not be skipped.", nb_chunk);

    uint32_t crc;
    if ((fread(&crc, sizeof(crc), 1, data)) == 0)
        errx(INVALID_FILE, "chunk %d has an invalid size.", nb_chunk);

    if (DEBUG)
    {
        printf("[CHUNK %3d] ", nb_chunk);
        printf("type: %s {0x%08x} | ", type_code, *(uint32_t *)type_code);
        printf("size: %d | ", size);
        printf("CRC: 0x%08x", crc);
        puts("");
    }
    nb_chunk++;

    return strcmp(type_code, "IEND");
}

void check_code(FILE *data)
{
    int32_t current_offset = ftell(data);
    fseek(data, 0, SEEK_END);
    uint32_t size_to_read = ftell(data) - current_offset;
    fseek(data, current_offset, SEEK_SET);

    char *s;
    if (!(s = calloc(size_to_read, sizeof(char))))
        errx(ERROR, "memory allocation failed.");

    if ((fread(s, size_to_read, 1, data)) == 0)
        errx(ERROR, "injected source code could not be read.");

    printf("Extracted source code:%s", s);

    FILE *output = fopen("output.c", "w+");
    if (!output)
        errx(ERROR, "could not open output.c");

    fprintf(output, "%s", s);
}

int main(int argc, char **argv)
{
    if (argc != 2)
        errx(WRONG_ARG, "Usage: %s <png_file>", argv[0]);

    FILE *data = fopen(argv[1], "rb");
    if (!data)
        errx(ERROR, "could not open %s", argv[1]);

    verify_signature(data);

    do {} while (get_chunk(data));

    check_code(data);

    fclose(data);
    return SUCCESS;
}
